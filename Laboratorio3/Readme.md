# Laboratorio 3
## Desarrollo de software 3
## Andres Mauricio Rojas Espinoza
---
Crear una REST API simple usando HTTP, que permita hacer un CRUD de tareas. Siga las siguientes instrucciones:

- Una tarea tiene que tener su t�tulo, descripci�n, fecha de creaci�n, fecha de finalizaci�n y estado.

- Debe estar hecha en C#.
- Puede usar cualquier framework 
- Puede usar cualquier base de datos siempre y cuando sea relacional (PostgreSQL, MySQL, SQL Server)
- Debe tener validaciones (por ejemplo, t�tulo requerido, fecha de finalizaci�n no puede ser anterior a la fecha de creaci�n)
- Se valorar� el uso de buenas practicas para la implementaci�n de APIs (definici�n de recursos, uso correcto de m�todos HTTP, uso correcto de c�digos de estado en las respuestas, etc).
- Se debe contar con un archivo Readme que tenga una breve descripci�n de la aplicacion y que explique como se puede ejecutarla.

---
# nota

Para antes de ejecutar el proyecto se debe migrar el modelo a la base de datos para ello nesesitamso acceder a la **consola del administrador de paquetes nuget** una ves en la consola:
1) ejecutamos el comando ```Add-Migration Initial``` 
![Add-Migration Initial](ReadmeResources/Add-Migration.png)
en caso de error verificar que la cadena de conexion en el appsettings.json sea correcta con la que tine su SQL server expres
![cadena de conexion sql server](ReadmeResources/CadenaDEConexion.png)
2) si todo salio bien procedemos a aplicar los cambios en la base de datos con el comando ```Update-database ```
como podemos ver la migracion del modelo de la db fue correcta y se ve reflejada en el sql server
![Update-database](ReadmeResources/Update-database.png)
---

# Recursos utilizados
Para desarrollar esta API estoy utilizando los siguientes frameworks:
- [Net core 8](https://dotnet.microsoft.com/es-es/download/dotnet/8.0)
- [Entity Framework Core](https://learn.microsoft.com/es-es/ef/core/)
- Entity Framework core SQL server
- [Entity Framework core tools](https://learn.microsoft.com/en-us/ef/core/cli/)
**Paquetes instalados**
![Paquetes instalados](ReadmeResources/Nugets.png)
- para este proyecto utilice  [SQL Server Express](https://www.microsoft.com/es-es/sql-server/sql-server-downloads)
---
# pruebas de la ejecucion correcta de la API
para comensar ejecutamos la api, en este caso nuestro programa al crearlo con net core 8 nos da la opcion de utilizar Swagger con esto podremos consumir la API a demas que nos sirve como documentacion
como podemos ver contamos con todas las operaciones crud
![Swagger1](ReadmeResources/Swagger1.png)
## POST
provemos las distintas operaciones empesando por registrar algunos datos (**POST**)
![SwaggerPost1](ReadmeResources/SwaggerPost1.png)
ejecutamos el post y como podemos ver se ejecuto sin problemas
![SwaggerPost2](ReadmeResources/SwaggerPost2.png)
## GET
comprobaremos si los datos que agregamos anterior mente nos aparecen realizando una operacion de GET
y como podemos observar los datos agregados anteriormente se hicieron efectivos.
![get1](ReadmeResources/get1.png)
## GET por id
para probar esto agregue una segunda tarea para mostrala
y como podemos ver nos muestra la tarea con ID 2
![getid](ReadmeResources/getid.png)
## PUT
ahora probaremos las actualizaciones, para eso modificare el estado de la tarea con Id 1 a terminada
![put1](ReadmeResources/put1.png)
verificando con un get por id vemos que efectivamente se actualizo
![put2](ReadmeResources/put2.png)
## DELETE
para probar el DELETE eliminaremos la tarea con ID 1
![delete1](ReadmeResources/delete1.png)
ahora haremos un GET para ver si todavia tenemos esta tarea
![delete2](ReadmeResources/delete2.png)
como podemos ver la tarea con id 1 fue eliminada.
comprovaremos la persistencia en la base de datos con SQL management studio
![Management](ReadmeResources/Management.png)
como podemos ver solo tenemos la tarea con Id 2
