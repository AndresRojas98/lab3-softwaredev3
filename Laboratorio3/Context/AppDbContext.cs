﻿using Laboratorio3.Models;
using Microsoft.EntityFrameworkCore;

namespace Laboratorio3.Context
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options): base (options) 
        {
             
        }
        public DbSet<Tasks> Tasks { get; set; }

    }
}
