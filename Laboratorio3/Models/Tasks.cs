﻿namespace Laboratorio3.Models
{
    public class Tasks
    {
        public int Id { get; set; }
        public required string Title { get; set; }
        public required string Description { get; set; }
        public required DateTime CreationDate { get; set; }
        public required DateTime FinishDate { get; set;}
        public required string Status { get; set; }
    }
}
